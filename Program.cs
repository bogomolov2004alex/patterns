using System;
using System.Collections;
using System.Collections.Generic;

namespace Program
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Пример использования паттерна Singleton
            ToyFactory toyFactory = ToyFactory.GetInstance();
            toyFactory.ProduceToy("Teddy Bear");

            // Пример использования паттерна Decorator
            IToy dollWithSound = new ToyWithSound(new Doll());
            dollWithSound.Produce();

            IToy carWithSoundAndLights = new ToyWithLights(new ToyWithSound(new Car()));
            carWithSoundAndLights.Produce();

            // Пример использования паттерна Iterator
            ToyCollection toyCollection = new ToyCollection();
            toyCollection.AddToy(toy1);
            toyCollection.AddToy(musicalToy);
            toyCollection.AddToy(lightUpToy);

            IIterator iterator = toyCollection.CreateIterator();
            while (iterator.HasNext())
            {
                Toy toy = (Toy)iterator.Next();
                Console.WriteLine("Элемент коллекции: " + toy.Name);
            }
        }
    }
    
    // Паттерн Singleton
    public class ToyFactory
    {
        private static ToyFactory instance;

        private ToyFactory() { }

        public static ToyFactory GetInstance()
        {
            if (instance == null)
            {
                instance = new ToyFactory();
            }
            return instance;
        }

        public void ProduceToy(string name)
        {
            Console.WriteLine("Producing toy: " + name);
        }
    }

    // Паттерн Decorator
    public abstract class ToyDecorator : IToy
    {
        protected IToy toy;

        public ToyDecorator(IToy toy)
        {
            this.toy = toy;
        }

        public virtual void Produce()
        {
            toy.Produce();
        }
    }

    public interface IToy
    {
        void Produce();
    }

    public class Doll : IToy
    {
        public void Produce()
        {
            Console.WriteLine("Producing a doll.");
        }
    }

    public class Car : IToy
    {
        public void Produce()
        {
            Console.WriteLine("Producing a car.");
        }
    }

    public class ToyWithSound : ToyDecorator
    {
        public ToyWithSound(IToy toy) : base(toy) { }

        public override void Produce()
        {
            base.Produce();
            Console.WriteLine("Adding sound to the toy.");
        }
    }

    public class ToyWithLights : ToyDecorator
    {
        public ToyWithLights(IToy toy) : base(toy) { }

        public override void Produce()
        {
            base.Produce();
            Console.WriteLine("Adding lights to the toy.");
        }
    }
    // Iterator паттерн
    public interface IToyCollection
    {
        IIterator CreateIterator();
    }

    public interface IIterator
    {
        bool HasNext();
        object Next();
    }

    // Коллекция игрушек
    public class ToyCollection : IToyCollection
    {
        private List<Toy> toys = new List<Toy>();

        public void AddToy(Toy toy)
        {
            toys.Add(toy);
        }

        public IIterator CreateIterator()
        {
            return new ToyIterator(this);
        }

        // Вложенный класс итератора
        private class ToyIterator : IIterator
        {
            private ToyCollection toyCollection;
            private int currentIndex;

            public ToyIterator(ToyCollection toyCollection)
            {
                this.toyCollection = toyCollection;
                currentIndex = 0;
            }

            public bool HasNext()
            {
                return currentIndex < toyCollection.toys.Count;
            }

            public object Next()
            {
                if (HasNext())
                {
                    Toy toy = toyCollection.toys[currentIndex];
                    currentIndex++;
                    return toy;
                }
                throw new InvalidOperationException("No more toys");
            }
        }
    }
}
